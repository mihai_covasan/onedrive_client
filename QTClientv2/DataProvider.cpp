#include "DataProvider.h"
#include <iterator>
#include <sstream>
#include <fstream>

DataProvider::DataProvider(const User& user, std::string ipAddress, int port)
	: ipAddress(ipAddress), port(port), user(user)
{
	socket.Connect(ipAddress, port);
}

bool DataProvider::sendData(const CustomMsgTypes msg, const std::string& data = "")
{
	std::string processedData = "";
	bool result;

	switch (msg)
	{
	case CustomMsgTypes::Authenticate:
	{
		processedData = "3 " + data;
		result = socket.Send(processedData.c_str(), processedData.size());
		break;
	}
	case CustomMsgTypes::Register:
	{
		processedData = "4 " + data;
		result = socket.Send(processedData.c_str(), processedData.size());
		break;
	}
	case CustomMsgTypes::FilesList:
	{
		processedData = "0 " + data;
		break;
	}
	case CustomMsgTypes::SendFile:
	{
		processedData = "2 " + data;
		result = socket.Send(processedData.c_str(), processedData.size());
		break;
	}
	case CustomMsgTypes::GetLocal:
	{
		processedData = "6 " + data;
		result = socket.Send(processedData.c_str(), processedData.size());
		break;
	}
	case CustomMsgTypes::RemoveFile:
	{
		processedData = "1 " + data;
		break;
	}
	case CustomMsgTypes::CheckExistingFolder:
		processedData = "5 " + data;
		break;
	}


	if (result)
	{
		std::array<char, 512> receiveBuffer;
		int recieved;
		std::stringstream ss;
		socket.Receive(receiveBuffer.data(), receiveBuffer.size(), recieved);
		//std::cout << "Received: \n";
		std::copy(receiveBuffer.begin(), receiveBuffer.begin() + recieved, std::ostream_iterator<char>(ss, ""));
		ss >> authenticate_response;
		//std::cout << authenticate_response;
		return true;
	}
	else
	{
		return false;
	}
}

bool DataProvider::registerUser()
{
	auto flag = sendData(CustomMsgTypes::Register, user.GetName());
	if (flag == false)
	{
		std::cout << "Error sending data!";
		return false;
	}
	return true;
}

bool DataProvider::connectUser()
{
	auto flag = sendData(CustomMsgTypes::Authenticate, user.GetName());
	if (flag == false)
	{
		std::cout << "Error sending data!";
		return false;
	}
	return true;
}

bool DataProvider::sendFile(std::string path_file, std::string name)
{

	std::ifstream t(path_file);
	std::stringstream file_data;
	file_data << t.rdbuf();

	auto flag = sendData(CustomMsgTypes::SendFile, file_data.str());
	return false;
}

bool DataProvider::sendFileLocal(std::string path_file, std::string name)
{

	std::ifstream t(path_file);
	std::stringstream file_data;
	file_data << t.rdbuf();

	auto flag = sendData(CustomMsgTypes::GetLocal, file_data.str());
	return false;
}

bool DataProvider::sendFolder()
{
	return false;
}

bool DataProvider::removeFolder()
{
	auto flag = sendData(CustomMsgTypes::RemoveFile);
	if (flag == false)
	{
		std::cout << "Error sending data!";
		return false;
	}
	return true;
}

bool DataProvider::getFolder()
{
	auto flag = sendData(CustomMsgTypes::SendFile);
	//aici ar trebui sa astepte

	if (flag == false)
	{
		std::cout << "Error sending data!";
		return false;
	}
	return true;
}