#pragma once
#include <string>
#include <vector>
#include <array>
#include "User.h"
#include "TcpSocket.h"

enum class CustomMsgTypes : uint32_t
{
	FilesList,
	RemoveFile,
	SendFile,
	GetLocal,
	Authenticate,
	Register,
	CheckExistingFolder,
};

class DataProvider
{
private:

	User user;
	std::string ipAddress;
	int port;
	TcpSocket socket;
	bool sendData(const CustomMsgTypes msg, const std::string& data);

public:
	// Initialize Client
	std::string authenticate_response;
	DataProvider(const User& user, std::string ipAddress, int port);

	//Messages
	bool registerUser();
	bool sendFile(std::string path_file, std::string name);
	bool sendFileLocal(std::string path_file, std::string name);
	bool sendFolder();
	bool removeFolder();
	bool connectUser();
	bool getFolder();
};
