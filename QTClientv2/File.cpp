#include "File.h"
File::File()
{
}

File::File(const std::string& path, const std::string& name, uint16_t size) : name{ name }, size{ size }, path(path)
{
}

File::File(const std::string& name, uint16_t size) : name{ name }, size{ size }
{}

const std::string& File::getNume()
{
	return name;
}

uint16_t File::getSize() const
{
	return size;
}

std::ostream& operator<<(std::ostream& f, const File& file)
{
	return f << file.name << file.size;
}



std::istream& operator>>(std::istream& g, File& file)
{
	return g >> file.name >> file.size;
}