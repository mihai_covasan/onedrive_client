#pragma once
#include<string>
#include <cstdint>
#include<iostream>

class File {
public:
	std::string name;
	std::uint16_t size;
	std::string path;

public:
	File();
	File(const std::string& path, const std::string& name, uint16_t size);
	File(const std::string& name, uint16_t size);
	const  std::string& getNume();
	uint16_t getSize() const;
	friend std::ostream& operator<<(std::ostream& f, const File& file);
	friend std::istream& operator>>(std::istream& g, File& file);


};
