#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QTClientv2.h"

class QTClientv2 : public QMainWindow
{
    Q_OBJECT

public:
    QTClientv2(QWidget *parent = Q_NULLPTR);

private:
    Ui::QTClientv2Class ui;
};
