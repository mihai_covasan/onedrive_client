#include "User.h"

User::User()
{
    this->name = "";
    this->userFile = File();
}

User::User(const std::string& name) : name(name)
{
}

std::string User::GetName()
{
    return name;
}

void User::SetName(std::string name)
{
    this->name = name;
}

File User::GetFile()
{
    return File();
}

void User::SetFile(File)
{
}
