#pragma once
#include <string>
#include "File.h"
class User
{
private:
	std::string name;
	File userFile;

public:
	User();
	User(const std::string&);

	//Getters and setters
	std::string GetName();
	void SetName(std::string);
	File GetFile();
	void SetFile(File);
};
