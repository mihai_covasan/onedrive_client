#include "app.h"
#include <qstackedwidget.h>
#include <QVBoxLayout>
#include <qcombobox.h>
#include <QCombobox>
#include <QWidget>
#include <QLabel>
#include <QDebug>
#include <QMessageBox>
#include <iostream>
#include <QTreeView>
#include <qstackedwidget.h>
#include <QVBoxLayout>
#include <qcombobox.h>
#include <QCombobox>
#include "qtreewidget.h"
#include <QListView>
#include <QScrollBar>
#include <QFileDialog>
#include<filesystem>
#include<qcompleter.h>

namespace fs = std::filesystem;

app::app(std::string username, QWidget *parent)
	: QMainWindow(parent)
{
    ui.setupUi(this);
    QPalette palette;
    palette.setBrush(QPalette::Window, Qt::white);
    this->setPalette(palette);
    this->setWindowIcon(QIcon("icon.png"));
    this->setWindowTitle("OneDrive");
   
    user = username;
   
    std::wstring path = fs::current_path();
    std::string pathClient(path.begin(), path.end());

    pathClient = "../Server/DataStorage";
    pathClient += '/';
    pathClient += username;

    model = new QFileSystemModel(this);
    model->setReadOnly(false);
    QString qstr = QString::fromStdString(pathClient);
    model->setRootPath(qstr);
    ui.treeView->setModel(model);
    ui.treeView->verticalScrollBar()->setEnabled(true);
    ui.treeView->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui.treeView->setRootIndex(model->index(qstr));

    std::string pathClient2(path.begin(), path.end());

    pathClient2 = "./DataStorage";
    pathClient2 += '/';
    pathClient2 += username;

    model = new QFileSystemModel(this);
    model->setReadOnly(false);
    qstr = QString::fromStdString(pathClient2);
    model->setRootPath(qstr);
    ui.treeView_2->setModel(model);
    ui.treeView_2->verticalScrollBar()->setEnabled(true);
    ui.treeView_2->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui.treeView_2->setRootIndex(model->index(qstr));

    std::string pathClient3(path.begin(), path.end());

    pathClient3 = "../Server/Trash";
    pathClient3 += '/';
    pathClient3 += username;

    model = new QFileSystemModel(this);
    model->setReadOnly(false);
    qstr = QString::fromStdString(pathClient3);
    model->setRootPath(qstr);
    ui.treeView_3->setModel(model);
    ui.treeView_3->verticalScrollBar()->setEnabled(true);
    ui.treeView_3->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    ui.treeView_3->setRootIndex(model->index(qstr));
    
    ui.treeView_3->setHidden(true);
    ui.TrashL->setHidden(true);
    ui.local_btn->setHidden(true);
    ui.delete_trash->setHidden(true);
    ui.DT->setHidden(true);
}

void app::on_trash_btn_clicked() {

    ui.treeView_3->setHidden(false);
    ui.treeView_2->setHidden(true);
    ui.TrashL->setHidden(false);
    ui.label_2->setHidden(true);
    ui.local_btn->setHidden(false);
    ui.trash_btn->setHidden(true);
    ui.delete_trash->setHidden(false);
    ui.DT->setHidden(false);
    ui.DS->setHidden(true);
    ui.delete_btn->setHidden(true);
}

void app::on_local_btn_clicked() {

    ui.treeView_3->setHidden(true);
    ui.treeView_2->setHidden(false);
    ui.TrashL->setHidden(true);
    ui.label_2->setHidden(false);
    ui.local_btn->setHidden(true);
    ui.trash_btn->setHidden(false);
    ui.delete_trash->setHidden(true);
    ui.DT->setHidden(true);
    ui.DS->setHidden(false);
    ui.delete_btn->setHidden(false);
}

void app::on_Add_clicked() {

    QCompleter* cmpt;
    model = new QFileSystemModel(this);
    cmpt = new QCompleter(model, this);
    model->setRootPath(QDir::rootPath());
    QString filter = "All File (.) ;; Text File(*.txt)";
    QString filename = QFileDialog::getOpenFileName(this, "Open a file", "C://");
    QFile file(filename);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        return;
    }
    QTextStream in(&file);
    QString text = in.readAll();
    sendFile(filename.toStdString(), user);

}

void app::sendFile(std::string path, std::string name)
{
    DataProvider dp(User(name), "192.168.2.114", 27015);
    dp.sendFile(path, name);
}

void app::on_DT_clicked()
{
    std::wstring pathOrigin = fs::current_path();

    QModelIndex index = ui.treeView_3->currentIndex();
    std::string name1 = model->fileName(index).toStdString();

    DataProvider dp(User(user), "192.168.2.114", 27015);
    dp.sendFile(name1, user);

    std::string pathClient = "../Server/Trash";
    pathClient += '/';
    pathClient += user;
    pathClient += '/';
    pathClient += name1;
    std::uintmax_t n1 = fs::remove(pathClient);
    fs::current_path(pathOrigin);

}

void app::on_DS_clicked()
{
    std::wstring pathOrigin = fs::current_path();

    QModelIndex index = ui.treeView_2->currentIndex();
    std::string name1 = model->fileName(index).toStdString();
    std::string name = name1;

    DataProvider dp(User(user), "192.168.2.114", 27015);
    dp.sendFileLocal(name1, user);
}

void app::on_delete_btn_clicked()
{

        std::wstring pathOrigin = fs::current_path();

        QModelIndex index = ui.treeView_2->currentIndex();
        std::string name = model->fileName(index).toStdString();
        std::string pathClient = "./DataStorage";
        pathClient += '/';
        pathClient += user;
        pathClient += '/';
        pathClient += name;
        std::uintmax_t n1 = fs::remove(pathClient);
        fs::current_path(pathOrigin);
}

void app::on_delete_trash_clicked()
{

    std::wstring pathOrigin = fs::current_path();

    QModelIndex index = ui.treeView_3->currentIndex();
    std::string name = model->fileName(index).toStdString();
    std::string pathClient = "../Server/Trash";
    pathClient += '/';
    pathClient += user;
    pathClient += '/';
    pathClient += name;
    std::uintmax_t n1 = fs::remove(pathClient);
    fs::current_path(pathOrigin);
}

void app::on_deleteS_clicked()
{

    std::wstring pathOrigin = fs::current_path();

    QModelIndex index = ui.treeView->currentIndex();
    std::string name = model->fileName(index).toStdString();
    std::string pathClient = "../Server/DataStorage";
    pathClient += '/';
    pathClient += user;
    pathClient += '/';
    pathClient += name;

    std::string pathTrash = "../Server/Trash";
    pathTrash += '/';
    pathTrash += user;
    pathTrash += '/';
    pathTrash += name;
    rename(pathClient.c_str(), pathTrash.c_str());

    std::uintmax_t n1 = fs::remove(pathClient);
    fs::current_path(pathOrigin);

    std::string pathClient2 = "./DataStorage";
    pathClient2 += '/';
    pathClient2 += user;
    pathClient2 += '/';
    pathClient2 += name;
    std::uintmax_t n2 = fs::remove(pathClient2);
    fs::current_path(pathOrigin);

}