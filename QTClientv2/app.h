#pragma once

#include <QWidget>
#include <QtWidgets/QMainWindow>
#include "ui_app.h"
#include "clientapp.h"
#include <qfilesystemmodel.h>
#include "User.h"
#include "DataProvider.h"
#include "TcpSocket.h"

class app : public QMainWindow
{
	Q_OBJECT

public:
	app(std::string username, QWidget *parent = Q_NULLPTR);
	void sendFile(std::string path, std::string name);

private slots:
	void on_Add_clicked();
	void on_delete_btn_clicked();
	void on_deleteS_clicked();
	void on_trash_btn_clicked();
	void on_local_btn_clicked();
	void on_delete_trash_clicked();
	void on_DT_clicked();
	void on_DS_clicked();

private:
	Ui::app ui;
	std::string user;
	QFileSystemModel *model;
	
};
