#include "clientapp.h"
#include <qstackedwidget.h>
#include <QVBoxLayout>
#include <qcombobox.h>
#include <QCombobox>
#include <QWidget>
#include <QLabel>
#include <QDebug>
#include <QMessageBox>
#include <iostream>
#include<filesystem>

namespace fs = std::filesystem;

ClientApp::ClientApp(QWidget *parent)
    : QMainWindow(parent)
{
	ui.setupUi(this);
	QPixmap bg("bck.png");
	bg = bg.scaled(this->size(), Qt::IgnoreAspectRatio);
	QPalette palette;
	palette.setBrush(QPalette::Window, bg);
	this->setPalette(palette);
	this->setWindowIcon(QIcon("icon.png"));
	this->setWindowTitle("OneDrive");
	ui.label_5->setHidden(true);
	ui.back->setHidden(true);
}


void ClientApp::on_logIn_clicked() {

	QString username = ui.lineEdit->text();
	std::string x = username.toStdString();
	DataProvider dp(User(x), "192.168.2.114", 27015);
	auto response = dp.connectUser();

	std::wstring path = fs::current_path();
	std::string pathClient(path.begin(), path.end());
	pathClient = "./DataStorage";
	pathClient += '/';
	pathClient += x;

	if (dp.authenticate_response.compare("True") == 0){

		if (!std::filesystem::exists(pathClient)) {
			QMessageBox msgBox;
			msgBox.setText("Local Folder does not exist! Folder will be created");
			msgBox.exec();
			fs::create_directories(pathClient);
		}
		this->close();
		app* home(new app(x));
		home->show();
	}
	else {
		QMessageBox msgBox;
		msgBox.setText("Username doesn't exist, please try again");
		msgBox.exec();
	}
}

void ClientApp::on_signUp_clicked() {
	ui.logIn->setHidden(true);
	ui.signUp->setHidden(true);
	ui.label_3->setHidden(true);
	ui.label_4->setHidden(true);
	ui.label_5->setHidden(false);
	ui.back->setHidden(false);

}

void ClientApp::on_back_clicked() {
	ui.logIn->setHidden(false);
	ui.signUp->setHidden(false);
	ui.label_3->setHidden(false);
	ui.label_4->setHidden(false);
	ui.label_5->setHidden(true);
	ui.back->setHidden(true);
}

void ClientApp::on_register_btn_clicked() {
	QString username = ui.lineEdit->text();
	std::string x = username.toStdString();

	DataProvider dp(User(x), "192.168.2.114", 27015);
	//auto pingServerResponse = dp.connectUser();

	auto registerUserResponse = dp.registerUser();

	if (registerUserResponse == true)
	{
		if (dp.authenticate_response.compare("True") == 0) {
			QMessageBox msgBox;
			msgBox.setText("Username created");
			msgBox.exec();
			this->close();
			app* home(new app(x));
			home->show();
		}
		else {
			QMessageBox msgBox;
			msgBox.setText("Username exist, please try again");
			msgBox.exec();
		}
	}
}