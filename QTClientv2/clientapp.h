#pragma once

#include <QtWidgets/QMainWindow>
#include <QPushButton>
#include "ui_clientapp.h"
#include "User.h"
#include "DataProvider.h"
#include "TcpSocket.h"
#include "app.h"

class ClientApp : public QMainWindow
{
    Q_OBJECT

public:
    ClientApp(QWidget *parent = Q_NULLPTR);

private slots:
    void on_logIn_clicked();
    void on_signUp_clicked();
    void on_register_btn_clicked();
    void on_back_clicked();

private:
    Ui::ClientAppClass ui;
};
