#include "clientapp.h"
#include <QtWidgets/QApplication>

void CreateClientConnection()
{
    User us("Guest");
    DataProvider dp(us, "192.168.2.114", 27015);
    dp.connectUser();
}

int main(int argc, char *argv[])
{
    CreateClientConnection();
    QApplication a(argc, argv);
    ClientApp w;
    w.show();
    return a.exec();
}
