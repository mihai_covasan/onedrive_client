/********************************************************************************
** Form generated from reading UI file 'QTClientv2.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QTCLIENTV2_H
#define UI_QTCLIENTV2_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_QTClientv2Class
{
public:
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *QTClientv2Class)
    {
        if (QTClientv2Class->objectName().isEmpty())
            QTClientv2Class->setObjectName(QString::fromUtf8("QTClientv2Class"));
        QTClientv2Class->resize(600, 400);
        menuBar = new QMenuBar(QTClientv2Class);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        QTClientv2Class->setMenuBar(menuBar);
        mainToolBar = new QToolBar(QTClientv2Class);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        QTClientv2Class->addToolBar(mainToolBar);
        centralWidget = new QWidget(QTClientv2Class);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        QTClientv2Class->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(QTClientv2Class);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        QTClientv2Class->setStatusBar(statusBar);

        retranslateUi(QTClientv2Class);

        QMetaObject::connectSlotsByName(QTClientv2Class);
    } // setupUi

    void retranslateUi(QMainWindow *QTClientv2Class)
    {
        QTClientv2Class->setWindowTitle(QCoreApplication::translate("QTClientv2Class", "QTClientv2", nullptr));
    } // retranslateUi

};

namespace Ui {
    class QTClientv2Class: public Ui_QTClientv2Class {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QTCLIENTV2_H
