/********************************************************************************
** Form generated from reading UI file 'app.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_APP_H
#define UI_APP_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_app
{
public:
    QGraphicsView *graphicsView;
    QTreeView *treeView;
    QTreeView *treeView_2;
    QLabel *label;
    QLabel *label_2;
    QPushButton *Add;
    QPushButton *delete_btn;
    QPushButton *DS;
    QPushButton *deleteS;
    QPushButton *trash_btn;
    QTreeView *treeView_3;
    QLabel *TrashL;
    QPushButton *local_btn;
    QPushButton *delete_trash;
    QPushButton *DT;

    void setupUi(QWidget *app)
    {
        if (app->objectName().isEmpty())
            app->setObjectName(QString::fromUtf8("app"));
        app->resize(680, 439);
        graphicsView = new QGraphicsView(app);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        graphicsView->setGeometry(QRect(0, 0, 151, 441));
        graphicsView->setStyleSheet(QString::fromUtf8("background-color:rgb(131, 154, 255);"));
        treeView = new QTreeView(app);
        treeView->setObjectName(QString::fromUtf8("treeView"));
        treeView->setGeometry(QRect(180, 110, 221, 311));
        treeView_2 = new QTreeView(app);
        treeView_2->setObjectName(QString::fromUtf8("treeView_2"));
        treeView_2->setGeometry(QRect(420, 110, 231, 311));
        label = new QLabel(app);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(180, 80, 51, 16));
        QFont font;
        font.setPointSize(12);
        font.setBold(true);
        label->setFont(font);
        label_2 = new QLabel(app);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(420, 80, 51, 16));
        label_2->setFont(font);
        Add = new QPushButton(app);
        Add->setObjectName(QString::fromUtf8("Add"));
        Add->setGeometry(QRect(0, 130, 151, 24));
        delete_btn = new QPushButton(app);
        delete_btn->setObjectName(QString::fromUtf8("delete_btn"));
        delete_btn->setGeometry(QRect(0, 190, 151, 24));
        DS = new QPushButton(app);
        DS->setObjectName(QString::fromUtf8("DS"));
        DS->setGeometry(QRect(0, 220, 151, 24));
        deleteS = new QPushButton(app);
        deleteS->setObjectName(QString::fromUtf8("deleteS"));
        deleteS->setGeometry(QRect(0, 160, 151, 24));
        trash_btn = new QPushButton(app);
        trash_btn->setObjectName(QString::fromUtf8("trash_btn"));
        trash_btn->setGeometry(QRect(0, 300, 151, 24));
        treeView_3 = new QTreeView(app);
        treeView_3->setObjectName(QString::fromUtf8("treeView_3"));
        treeView_3->setGeometry(QRect(420, 110, 231, 311));
        TrashL = new QLabel(app);
        TrashL->setObjectName(QString::fromUtf8("TrashL"));
        TrashL->setGeometry(QRect(420, 80, 51, 16));
        TrashL->setFont(font);
        local_btn = new QPushButton(app);
        local_btn->setObjectName(QString::fromUtf8("local_btn"));
        local_btn->setGeometry(QRect(0, 300, 151, 24));
        delete_trash = new QPushButton(app);
        delete_trash->setObjectName(QString::fromUtf8("delete_trash"));
        delete_trash->setGeometry(QRect(0, 270, 151, 24));
        DT = new QPushButton(app);
        DT->setObjectName(QString::fromUtf8("DT"));
        DT->setGeometry(QRect(0, 330, 151, 24));

        retranslateUi(app);

        QMetaObject::connectSlotsByName(app);
    } // setupUi

    void retranslateUi(QWidget *app)
    {
        app->setWindowTitle(QCoreApplication::translate("app", "app", nullptr));
        label->setText(QCoreApplication::translate("app", "Server", nullptr));
        label_2->setText(QCoreApplication::translate("app", "Local", nullptr));
        Add->setText(QCoreApplication::translate("app", "+ Add", nullptr));
        delete_btn->setText(QCoreApplication::translate("app", "Delete Local", nullptr));
        DS->setText(QCoreApplication::translate("app", "Download from Server", nullptr));
        deleteS->setText(QCoreApplication::translate("app", "Delete Server", nullptr));
        trash_btn->setText(QCoreApplication::translate("app", "Trash", nullptr));
        TrashL->setText(QCoreApplication::translate("app", "Trash", nullptr));
        local_btn->setText(QCoreApplication::translate("app", "Local", nullptr));
        delete_trash->setText(QCoreApplication::translate("app", "Delete From Trash", nullptr));
        DT->setText(QCoreApplication::translate("app", "Download from Trash", nullptr));
    } // retranslateUi

};

namespace Ui {
    class app: public Ui_app {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_APP_H
