/********************************************************************************
** Form generated from reading UI file 'clientapp.ui'
**
** Created by: Qt User Interface Compiler version 5.15.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CLIENTAPP_H
#define UI_CLIENTAPP_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_ClientAppClass
{
public:
    QWidget *centralWidget;
    QLineEdit *lineEdit;
    QLabel *label_2;
    QPushButton *logIn;
    QLabel *label_3;
    QPushButton *signUp;
    QPushButton *register_btn;
    QLabel *label_4;
    QLabel *label_5;
    QPushButton *back;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *ClientAppClass)
    {
        if (ClientAppClass->objectName().isEmpty())
            ClientAppClass->setObjectName(QString::fromUtf8("ClientAppClass"));
        ClientAppClass->resize(600, 400);
        centralWidget = new QWidget(ClientAppClass);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(200, 140, 181, 22));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(200, 120, 61, 16));
        logIn = new QPushButton(centralWidget);
        logIn->setObjectName(QString::fromUtf8("logIn"));
        logIn->setGeometry(QRect(250, 170, 81, 24));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(230, 230, 131, 16));
        signUp = new QPushButton(centralWidget);
        signUp->setObjectName(QString::fromUtf8("signUp"));
        signUp->setGeometry(QRect(250, 250, 81, 24));
        register_btn = new QPushButton(centralWidget);
        register_btn->setObjectName(QString::fromUtf8("register_btn"));
        register_btn->setGeometry(QRect(250, 170, 81, 24));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(240, 40, 101, 51));
        QPalette palette;
        QBrush brush(QColor(255, 255, 255, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush1(QColor(0, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        QBrush brush2(QColor(120, 120, 120, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        label_4->setPalette(palette);
        QFont font;
        font.setPointSize(26);
        font.setBold(true);
        label_4->setFont(font);
        label_4->setTextFormat(Qt::MarkdownText);
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(230, 40, 141, 51));
        QPalette palette1;
        palette1.setBrush(QPalette::Active, QPalette::Text, brush);
        palette1.setBrush(QPalette::Inactive, QPalette::Text, brush1);
        palette1.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        label_5->setPalette(palette1);
        label_5->setFont(font);
        label_5->setTextFormat(Qt::MarkdownText);
        back = new QPushButton(centralWidget);
        back->setObjectName(QString::fromUtf8("back"));
        back->setGeometry(QRect(10, 10, 41, 24));
        ClientAppClass->setCentralWidget(centralWidget);
        register_btn->raise();
        lineEdit->raise();
        label_2->raise();
        logIn->raise();
        label_3->raise();
        signUp->raise();
        label_4->raise();
        label_5->raise();
        back->raise();
        statusBar = new QStatusBar(ClientAppClass);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        ClientAppClass->setStatusBar(statusBar);

        retranslateUi(ClientAppClass);

        QMetaObject::connectSlotsByName(ClientAppClass);
    } // setupUi

    void retranslateUi(QMainWindow *ClientAppClass)
    {
        ClientAppClass->setWindowTitle(QCoreApplication::translate("ClientAppClass", "ClientApp", nullptr));
        label_2->setText(QCoreApplication::translate("ClientAppClass", "Username:", nullptr));
        logIn->setText(QCoreApplication::translate("ClientAppClass", "Log In", nullptr));
        label_3->setText(QCoreApplication::translate("ClientAppClass", "Don't have an account?", nullptr));
        signUp->setText(QCoreApplication::translate("ClientAppClass", "Sign Up", nullptr));
        register_btn->setText(QCoreApplication::translate("ClientAppClass", "Register", nullptr));
        label_4->setText(QCoreApplication::translate("ClientAppClass", "Log In", nullptr));
        label_5->setText(QCoreApplication::translate("ClientAppClass", "Register", nullptr));
        back->setText(QCoreApplication::translate("ClientAppClass", "<-", nullptr));
    } // retranslateUi

};

namespace Ui {
    class ClientAppClass: public Ui_ClientAppClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CLIENTAPP_H
